# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=263
pkgrel=1
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-libarchive-c
	py3-magic
	"
makedepends="
	py3-docutils
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	bzip2
	cdrkit
	gzip
	libarchive-tools
	openssh-client-default
	py3-html2text
	py3-pytest
	py3-pytest-xdist
	unzip
	"
subpackages="$pkgname-pyc"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"

# secfixes:
#   256-r0:
#     - CVE-2024-25711

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# html test fails
	PYTHONDONTWRITEBYTECODE=1 \
	.testenv/bin/python3 -m pytest -n auto -k 'not test_diff'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e5c3372d83006eb8691f78571de8f7b0c02aa97341cbcbd81e6eaa35f603ed8ca2d28ed826ef758ce32f691846ac7016e078d8a9d9e2649b8056c7c3ed84a4b6  diffoscope-263.tar.gz
"
